require('dotenv').config()
const assert = require('assert');

const MovieDBClient = require('../src/MovieDBClient');

describe('MovieDBClient', () => {
  let client;

  before(() => {
    client = new MovieDBClient(process.env.API_KEY);
  });

  it('should get return movie details', async () => {
    const movieId = 76341;
    const movieRes = await client.getMovie(movieId);
    assert(movieRes.status, 200);
    const movie = await movieRes.json();
    assert(movie.id, movieId);
  });
});
