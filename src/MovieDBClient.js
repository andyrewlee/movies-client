const fetch = require('node-fetch');

const API_URL = 'https://api.themoviedb.org/3';

class MovieDBClient {
  constructor(apiKey) {
    this.apiKey = apiKey;
  }

  async getMovie(id) {
    const url = `${API_URL}/movie/${id}?api_key=${this.apiKey}`;

    try {
      const res = await fetch(url);
      return res;
    } catch (err) {
      throw new Error(err);
    }
  }
}

module.exports = MovieDBClient;
